package main

import (
	"bufio"
	"flag"
	"fmt"
	"image"
	"image/color"
	"log"
	"os"

	loadOptions "gitlab.com/okotek/loadoptions"
	okolog "gitlab.com/okotek/okolog"
	okonet "gitlab.com/okotek/okonet"
	types "gitlab.com/okotek/okotypes"

	"gocv.io/x/gocv"
)

var cfig types.ConfigOptions

func main() {

	sendoffTarget := flag.String("sendoffTarget", "localhost:8083", "Sendoff target.")
	recieverTarget := flag.String("recieverTarget", ":8082", "Port to listen for incoming on.")
	stamperPipelineCount := flag.Int("stamperPipelineCount", 1, "If 1, a simplified stamper pipeline is used. Otherwise, uses number of stampers provided. If <1, errors out.")
	flag.Parse()

	cfig := loadOptions.Load()
	okolog.LoggerIDInit(cfig)

	//cfig.InstanceIdentifier = okolog.InitStruct.Identifier

	stageOneChan := make(chan types.Frame)
	stageTwoChan := make(chan types.Frame)

	go okonet.SendoffHandlerCached(stageTwoChan, *sendoffTarget, 25)
	go okonet.ReceiverHandler(stageOneChan, *recieverTarget)

	okolog.MessageLogger(fmt.Sprintf("stamperPipelineCount: %d", *stamperPipelineCount), "LOG")

	if *stamperPipelineCount > 1 {
		okolog.MessageLogger("Starting single pipe stamper.", "LOG")
		for iter := 0; iter < *stamperPipelineCount; iter++ {
			go stamper(stageOneChan, stageTwoChan)
		}
	} else if *stamperPipelineCount == 1 {
		okolog.MessageLogger(fmt.Sprintf("Starting %d stampers.", *stamperPipelineCount), "LOG")
		go stamper(stageOneChan, stageTwoChan)
	} else {
		okolog.MessageLogger("Killing stamp machine due to sub-zero pipeline start.", "ERROR")
		fmt.Println("Sub single pipeline. Did you give a negative number for the stamperPipelineCount flag?")
		os.Exit(1)
	}

	//Prevent program from quitting
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func stamper(input chan types.Frame, output chan types.Frame) {

	offWhite := color.RGBA{200, 200, 200, 250}
	okolog.MessageLogger("Started Stamper", "LOG")

	for iter := range input {
		img, err := gocv.NewMatFromBytes(
			iter.Height,
			iter.Width,
			iter.Type,
			iter.ImDat,
		)

		if err != nil {
			log.Printf("Error in stamper: ", err)
		}

		//Iterate over all detections in the image and draw boxes on them.
		//This was copied from a website for shame
		for _, detection := range iter.ClassifierTags {

			if len(detection.Rects) > 0 {
				go okolog.MessageLogger("DETECTION FOUND", "LOG")
				for _, rct := range detection.Rects {
					gocv.Rectangle(&img, rct, offWhite, 3)
					size := gocv.GetTextSize(detection.ClassName, gocv.FontHersheyPlain, 1.2, 2)
					pt := image.Pt(rct.Min.X+(rct.Min.X/2)-(size.X/2), rct.Min.Y-2)
					gocv.PutText(&img, detection.ClassName, pt, gocv.FontHersheyPlain, 1.2, offWhite, 2)
				}
				go okolog.MessageLogger("Detections written.", "LOG")
			} else {
				fmt.Println("Zero len rectangle list")
				okolog.MessageLogger("NO DETECTIONS FOUND", "LOG")
				continue
			}
		}

		outputImg := types.CreateFrameFromExisting(img,
			iter.UserName,
			iter.UserPass,
			iter.ClassifierTags,
			iter.MotionPercentChange,
			iter.ImTime,
			iter.CamID,
			iter.UnitID,
			iter.UnitID)

		/*
			go func() {

				select {
				case output <- outputImg:
					//fmt.Println("Channel send worked.")
				default:
					fmt.Println("Channel send failed.")
					output <- outputImg
				}
			}()

			go func() {
				ed := iter
				ed.ImgName = ed.ImgName + ";no_rectangles"
				select {
				case output <- ed:
					//fmt.Println("Channel send worked.")
				default:
					fmt.Println("Channel send failed.")
					output <- ed
				}
			}()
		*/

		ed := iter
		ed.ImgName = ed.ImgName + ";no_rectanges"
		output <- ed
		output <- outputImg

		okolog.MessageLogger("Stamped new image.")

	}
}

//Currently unused. No fucking clue what this was for.
func getDetCount(input types.Frame) int {
	detectionCount := 0
	for _, iter := range input.ClassifierTags {
		if len(iter.Rects) < 1 {
			fmt.Println("Nothing there. Dump this")
			continue
		}
		detectionCount++
	}
	return detectionCount
}
