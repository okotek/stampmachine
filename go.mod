module gitlab.com/okotek/stampmachine

go 1.17

require (
	gitlab.com/okotek/loadoptions v0.0.0-00010101000000-000000000000
	gitlab.com/okotek/okolog v0.0.0-20220723033605-30385caa0e15
	gitlab.com/okotek/okonet v0.0.0-20220111050325-7dff580a79d7
	gitlab.com/okotek/okotypes v0.0.0-20220719220355-02567a19ce5f
	gocv.io/x/gocv v0.29.0
)

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	gitlab.com/okotech/loadoptions v0.0.0-20220719225702-2feb04b55a80 // indirect
	gitlab.com/okotek/sec v0.0.0-20220326221119-e10389566a6b // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
)

replace gitlab.com/okotek/okolog => ../okolog

replace gitlab.com/okotek/loadoptions => ../loadoptions

replace gitlab.com/okotek/okonet => ../okonet

replace gitlab.com/okotek/okotypes => ../okotypes
